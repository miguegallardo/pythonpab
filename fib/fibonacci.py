# -*- coding: utf-8 -*-
"""
Created on Sat Mar 11 10:06:35 2017 

@author: Migue
"""
    
def compute(month, rabbits):
    if month == 0 or month == 1:
        return 1
    elif month > 39:
        raise Exception("Months <= 40")

    if rabbits > 5:
        raise Exception("Rabbits <= 5")

    return (compute(month - 2, rabbits) * rabbits) + compute(month - 1, rabbits)