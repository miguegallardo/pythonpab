import unittest
from fib import fibonacci


class FibTest(unittest.TestCase):
    def test_base_flow(self):
        self.assertEquals(fibonacci.compute(1, 2), 1)
        self.assertEquals(fibonacci.compute(0, 3), 1)

    def test_good_flow(self):
        self.assertEquals(fibonacci.compute(5, 3), 40)
        self.assertEquals(fibonacci.compute(29, 2), 357913941)

    def test_wrong_flow(self):
        with self.assertRaises(Exception):
            fibonacci.compute(50, 1)

        with self.assertRaises(Exception):
            fibonacci.compute(4, 6)
